package com.vdt.filekrypt.ui;

import com.vdt.filekrypt.io.Kryption;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Terminal / CLI for filekrypt
 * @author vdtdev@gmail.com
 */
public class TerminalInterface {

    final String version = "0.1a";

    private ArgumentParser arguments;

    public TerminalInterface(String[] args) {
        showBanner();
        this.arguments = new ArgumentParser(args);
        if (this.arguments.isShowHelp()) {
            showHelp();
        } else {
            process();
        }
    }

    /**
     * Perform encryption operation
     */
    private void process() {
        try {
            Kryption.processFile(arguments.getInputFile(), arguments.getOutputFile(), arguments.getMask());
        } catch (IOException ex) {
            Logger.getLogger(TerminalInterface.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Unable to complete: " + ex.getMessage());
        }
    }

    private void showBanner() {
        System.out.println("filekrypt v" + version + " by vdtdev - (c) 2017, vdtdev all rights reserved.");
    }

    private void showHelp() {
        System.out.println("Usage:\n\n"
                + "filekrypt <source> <output> <mask> [-r]\n\n"
                + "\tsource\t\tSource input file\n"
                + "\toutput\t\tOutput encrypted file\n"
                + "\tmask\t\tMask bytes, space delimited in quotes (e.g. \"2 15 4 12\")\n"
        );
    }

}
