package com.vdt.filekrypt.ui;

import com.vdt.filekrypt.functions.MaskFunctions;

/**
 * 
 * Class for parsing command-line arguments
 * 
 * @author vdtdev@gmail.com
 */
public class ArgumentParser {

    private final int minimumArguments = 3;
    
    private String inputFile = null;
    private String outputFile = null;
    private int[] mask = {};
    private Boolean showHelp = false;
    private Boolean reverseMask = false;
   
    /**
     * Construct new argument parser and process arguments
     * @param arguments Array of arguments passed on command line
     */
    public ArgumentParser(String[] arguments){
        parse(arguments);
    }
    
    private int[] parseMask(String maskArg){
        maskArg = maskArg.replaceAll("\"", " ").trim();
        String[] maskParts = maskArg.split(" ");
        int[] tmpMask = new int[maskParts.length];
        for(int i=0;i<maskParts.length;i++){
            tmpMask[i] = Integer.parseInt(maskParts[i].trim());
        }
        return tmpMask;
    }
    
    /**
     * Parses arguments and populates property values accordingly
     * @param args Array of argument strings
     */
    private void parse(String[] args){
        if(args.length < minimumArguments) {
            showHelp = true;
        } else {
            inputFile = args[0];
            outputFile = args[1];
            mask = parseMask(args[2]);
        }
        
        if(args.length > minimumArguments){
            if(args[3].equalsIgnoreCase("-r")){
                reverseMask = true;
                mask = MaskFunctions.invertMask(mask);
            }
        }
    }
    
    public String getInputFile() { return this.inputFile; }
    public String getOutputFile() { return this.outputFile; }
    public int[] getMask() { return this.mask;}
    public Boolean isShowHelp() { return this.showHelp; }
    public Boolean isReverseMask() { return this.reverseMask; }
    
}
