package com.vdt.filekrypt.io;

import com.vdt.filekrypt.functions.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 *
 * @author vdtdev@gmail.com
 */
public class Kryption {
    
    /**
     * Apply krypt to data array using a given mask
     * @param data Data to encrypt
     * @param mask Encryption mask
     * @return Encrypted data
     */
    public static int[] process(int[] data, int[] mask){
        CryptFunction cryptFunc = new CryptFunction();
        return cryptFunc.apply(data, mask);
    }
    
    public static void processFile(String input, String output, int[] mask) throws FileNotFoundException, IOException{
        UnsignedFile.UfInputStream ufIn = new UnsignedFile.UfInputStream(input);
        UnsignedFile.UfOutputStream ufOut = new UnsignedFile.UfOutputStream(output);
        
        File f = new File(input);
        int fs = (int)f.length();
        
        int[] src = new int[fs];
        int[] dst = new int[fs];
        
        ufIn.read(src);
        dst = process(src, mask);
        
        ufOut.write(dst);
        
        ufIn.close();
        ufOut.close();
    }
    
}
