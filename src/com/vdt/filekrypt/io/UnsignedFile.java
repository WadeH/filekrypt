package com.vdt.filekrypt.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Input and Output streams for Unsigned byte files
 */
public class UnsignedFile {

    /**
     * Unsigned Byte File Input Stream
     */
    public static class UfInputStream extends FileInputStream {

        public UfInputStream(String name) throws FileNotFoundException {
            super(name);
        }

        /**
         * Cast byte values in an array to unsigned bytes in an int array
         *
         * @param bytes byte array containing source unsigned bytes
         * @param unsigned Array of ints containing unsigned bytes cast from
         * source array
         */
        private void bytesToUnsigned(byte[] bytes, int[] unsigned) {
            for (int idx = 0; idx < bytes.length; idx++) {
                unsigned[idx] = Byte.toUnsignedInt(bytes[idx]);
            }
        }

        /**
         * Read array of bytes from stream as unsigned bytes in int array
         *
         * @param unsigned Integer array to hold unsigned bytes
         * @return Last read byte or -1 for end of stream
         * @throws IOException
         */
        public int read(int[] unsigned) throws IOException {
            byte[] bytes = new byte[unsigned.length];
            int resultInt = super.read(bytes);
            bytesToUnsigned(bytes, unsigned);
            return resultInt;
        }

        /**
         * Read array of bytes from stream as unsigned bytes in int array
         *
         * @param unsigned Integer array to hold unsigned bytes
         * @param off Offset to read from
         * @param len Maximum number of bytes to read
         * @return Last read byte or -1 for end of stream
         * @throws IOException
         */
        public int read(int[] unsigned, int off, int len) throws IOException {
            byte[] bytes = new byte[unsigned.length];
            int resultInt = super.read(bytes, off, len);
            bytesToUnsigned(bytes, unsigned);
            return resultInt;
        }

    }

    /**
     * Unsigned byte file Output Stream
     */
    public static class UfOutputStream extends FileOutputStream {

        public UfOutputStream(String name) throws FileNotFoundException {
            super(name);
        }

        /**
         * Cast unsigned byte values in an int array to bytes in a byte array
         *
         * @param unsigned int array containing source unsigned bytes
         * @param bytes array of bytes to write cast values to
         */
        private byte[] unsignedToBytes(int[] unsigned) {
            byte[] bytes = new byte[unsigned.length];
            for (int idx = 0; idx < unsigned.length; idx++) {
                bytes[idx] = (byte) unsigned[idx];
            }
            return bytes;
        }

        /**
         * Write integer array containing unsigned bytes to stream as bytes
         *
         * @param unsignedBytes Array of unsigned byte integers (0..255)
         * @throws IOException
         */
        public void write(int[] unsignedBytes) throws IOException {
            // byte[] bytes = new byte[unsignedBytes.length];
            /* for(int i=0;i<unsignedBytes.length;i++){
             bytes[i]=(byte)unsignedBytes[i];
             } */
            byte[] bytes = unsignedToBytes(unsignedBytes);
            super.write(bytes);
        }

        /**
         * Writes <code>len</code> bytes at offset <code>off</code> to stream
         *
         * @param unsignedBytes Array of unsigned bytes to write
         * @param off Offset
         * @param len Length
         * @throws IOException
         */
        public void write(int[] unsignedBytes, int off, int len) throws IOException {
            byte[] bytes = unsignedToBytes(unsignedBytes);
            super.write(bytes, off, len);
        }

    }

}
