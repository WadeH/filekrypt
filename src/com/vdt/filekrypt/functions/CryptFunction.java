package com.vdt.filekrypt.functions;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * BiFunction for modifying unsigned byte array with an offset mask
 */
public class CryptFunction implements BiFunction<int[], int[], int[]> {

    /**
     * BiFunction used to modify source bytes with individual mask byte
     */
    protected class ByteMaskFunction implements BiFunction<Integer, Integer, Integer> {

        @Override
        public Integer apply(Integer source, Integer mask) {
            return (((int) source + (int) mask) % 256);
        }
    }

    

    /**
     * Return a chunk of an array
     * @param source Source array
     * @param offset starting offset
     * @param length chunk size
     * @return Chunk of {@code length} length starting at {@code offset}
     */
    private int[] takeChunk(int[] source, int offset, int length){
        int[] chunk = new int[length];
        System.arraycopy(source, offset, chunk, 0, length);
        return chunk;
    }
    
    /**
     * Alter bytes in a chunk using the ByteMaskFunction and a mask
     * @param chunk Chunk to modify
     * @param mask mask to use
     * @return Altered chunk
     */
    private int[] alterChunk(int[] chunk, int[] mask) {
        int[] altered = new int[chunk.length];
        ByteMaskFunction bmf = new ByteMaskFunction();
        for (int idx = 0; idx < chunk.length; idx++) {
            altered[idx] = bmf.apply(chunk[idx], mask[idx]);
        }
        return altered;
    }

    /**
     * Put a chunk into a larger array at a certain position
     * @param chunk
     * @param offset
     * @param destArray
     * @return Updated array
     */
    private int[] putChunk(int[] chunk, int offset, int[] destArray) {
        System.arraycopy(chunk, 0, destArray, offset, chunk.length);
        return destArray;
    }

    @Override
    public int[] apply(int[] source, int[] mask) {
        int[] result = new int[source.length];
        int steps = source.length / mask.length;
        for(int step = 0; step < steps; step++){
            int[] chunk = takeChunk(source, step * mask.length, mask.length);
            chunk = alterChunk(chunk, mask);
            System.arraycopy(chunk, 0, result, step * mask.length, chunk.length);
        }
        return result;
    }

}
