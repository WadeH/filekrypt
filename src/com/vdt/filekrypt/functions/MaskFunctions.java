package com.vdt.filekrypt.functions;

import java.util.function.Function;

/**
 *
 * Contains classes implementing Function interface related to Masks
 * 
 * @author vdtdev@gmail.com
 */
public class MaskFunctions {
    
    /**
     * Implements Function for reversing individual mask bytes to their inverse value
     */
    protected static class ReverseMaskByteFunction implements Function<Integer, Integer> {

        @Override
        public Integer apply(Integer source) {
            return (-source % 256);
        }
    }
    
    /**
     * Implement Function for reversing entire byte mask
     */
    protected static class ReverseMaskFunction implements Function<int[], int[]> {

        @Override
        public int[] apply(int[] t) {
            ReverseMaskByteFunction rmbFunc = new ReverseMaskByteFunction();
            int[] result = new int[t.length];
            for(int i=0;i<t.length;i++){
                result[i] = rmbFunc.apply(t[i]);
            }
            return result;
        }
        
    }
    
    /**
     * Invert a mask that can be used to reverse a krypt
     * @param mask Mask bytes to inverse
     * @return Array of inverted mask bytes
     */
    public static int[] invertMask(int[] mask){
        ReverseMaskFunction rmfunc = new ReverseMaskFunction();
        return rmfunc.apply(mask);
    }
    
}
