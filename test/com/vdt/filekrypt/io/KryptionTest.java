/*
 * The MIT License
 *
 * Copyright 2017 vdtdev@gmail.com.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.vdt.filekrypt.io;

import com.vdt.filekrypt.functions.MaskFunctions;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *  
 *  Unit Tests for Kryption class
 * 
 * @author vdtdev@gmail.com
 */
public class KryptionTest {
    
    public KryptionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of process method, of class Kryption.
     */
    @Test
    public void testProcess() {
        
        System.out.println("process");
        int[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] mask = {2, 4, 1, 3, 4};
        int[] expResult = {3,6,4,7,9,8,11,9,12,14};

        int[] result = Kryption.process(data, mask);
       
        assertArrayEquals(expResult, result);
    }
    
    /**
     * Test of process method (inverted), of class Kryption.
     */
    @Test
    public void testProcessInverse() {
        
        System.out.println("process");
        int[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        int[] mask = {2, 4, 1, 3, 4};
        int[] expResult = {3,6,4,7,9,8,11,9,12,14};
        
        int[] inverseMask = MaskFunctions.invertMask(mask);
        
        int[] result = Kryption.process(data, mask);
        
        int[] reverseResult = Kryption.process(result, inverseMask);
        
        assertArrayEquals(expResult, result);
        assertArrayEquals(data, reverseResult);

    }

    /**
     * Test of processFile method, of class Kryption.
     */
    @Test
    public void testProcessFile() throws Exception {
        System.out.println("processFile");
        String input = "";
        String output = "";
        int[] mask = null;
        Kryption.processFile(input, output, mask);
        // TODO review the generated test code and remove the default call to fail.
        fail("Not yet implemented.");
        
    }
    
}
