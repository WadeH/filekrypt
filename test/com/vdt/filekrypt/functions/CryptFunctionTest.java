/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vdt.filekrypt.functions;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vdtdev@gmail.com
 */
public class CryptFunctionTest {
    
    public CryptFunctionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of apply method, of class CryptFunction.
     */
    @Test
    public void testApply() {
        System.out.println("apply");
        int[] source = {1, 2, 3, 4};
        int[] mask = {5, 6};
        CryptFunction instance = new CryptFunction();
        int[] expResult = {6, 8, 8, 10};
        int[] result = instance.apply(source, mask);
        assertArrayEquals(expResult, result);
        
    }
    
}
