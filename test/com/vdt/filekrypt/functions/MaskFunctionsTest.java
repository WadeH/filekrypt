package com.vdt.filekrypt.functions;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Unit Tests for Mask Functions
 * 
 * @author vdtdev@gmail.com
 */
public class MaskFunctionsTest {
    
    public MaskFunctionsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of invertMask method, of class MaskFunctions.
     */
    @Test
    public void testInvertMask() {
        System.out.println("invertMask");
        int[] mask = {1,2,3};
        int[] expResult = {(-1)%256, -2%256, -3%256};
        int[] result = MaskFunctions.invertMask(mask);
        assertArrayEquals(expResult, result);
    }
    
}
