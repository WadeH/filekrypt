# filekrypt

Simple shift-based encryption tool.

- Shifts bytes in input file using a mask of bytes (e.g. ``2 255 13 12``)
- Has option for reversing encrypted file using original mask
- Simple command line interface

- Unit Tests use JUnit 4x; project includes NetBeans .properties file specifying library version

- Released under MIT license

by Wade H (vdtdev@gmail.com)